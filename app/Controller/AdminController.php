<?php

namespace  Controller;

use Model\PostModel;
use \W\Controller\Controller;

class AdminController extends Controller
{
    /**
     * Affichage du formulaire
     */
    public function form($error = null) {
        $this->show('admin/form', ['error' => $error]);
    }

    /**
     * Upload du fichier json.
     * Traitement de celui-ci
     */
    public function upload() {
        // vérifier l'existence du fichier
        if (!isset($_FILES['file'])) {
            return $this->form("Erreur lors du chargement du fichier.");
        }
        // récupérer le fichier uploadé
        $json = file_get_contents($_FILES['file']['tmp_name']);

        $postDatas = json_decode($json, true);
        $postModel = new PostModel();

        // parcours des posts
        $result = array(
            'inserted' => array(),
            'updated' => array(),
        );
        foreach ($postDatas as $postData) {
            // vérifier l'existence préalable du post
            $existingPost = $postModel->find($postData['id']);
            if ($existingPost) {
                // il existe, le mettre à jour
                $result['updated'][] = $postModel->update($postData, $postData['id']);
            } else {
                // il n'existe pas, l'insérer
                $result['inserted'][] = $postModel->insert($postData);
            }
        }

        $this->show('admin/upload', array('result' => $result));
    }
}