<?php

namespace Controller;

use Model\PostModel;
use \W\Controller\Controller;

class PostRestController extends Controller
{
    /**
     * @param $page integer
     */
    public function getList($page) {
        $postModel = new PostModel();
        $post_per_scroll = getApp()->getConfig('post_per_scroll');
        $posts = $postModel->findAll('date_add', 'DESC', $post_per_scroll, $page * $post_per_scroll);

        $this->showJson($posts);
    }

    /**
     * @param $textSearch string
     */
    public function search($textSearch) {
        $postModel = new PostModel();
        // on pourrait améliorer la recherche en ajoutant un retour par ordre décroissant de pertinence
        $post_per_scroll = getApp()->getConfig('post_per_scroll');
        $posts = $postModel->searchWithPagination('date_add', 'DESC', $post_per_scroll, 0, array(
            'title' => $textSearch
        ));

        $this->showJson($posts);
    }
}