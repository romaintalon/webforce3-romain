<?php

namespace Controller;

use Model\PostModel;
use \W\Controller\Controller;

class PostController extends Controller
{
    /**
     * Page d'accueil,
     * affiche les premiers résultats
     */
    public function home() {
        $postModel = new PostModel();
        $posts = $postModel->findAll('date_add', 'DESC', getApp()->getConfig('home_post_count'), 1);

        $this->show('post/home', [
            'posts' => $posts,
        ]);
    }

    /**
     * Recherche sur les articles
     * @param int $page
     */
    public function search($page = 1)
    {
        $textSearch = filter_input(INPUT_GET, 'textSearch');
        $search = array();
        if ($textSearch) {
            $search = array(
                'title' => $textSearch,
                'content' => $textSearch,
                'author' => $textSearch,
            );
        }
        $postModel = new PostModel();
        $post_per_page = getApp()->getConfig('post_per_page');
        $posts = $postModel->searchWithPagination('date_add', 'DESC', $post_per_page, ($page -1) * $post_per_page,
                $search, 'OR'
        );
        $this->show('post/search', [
            'posts' => $posts,
            'page' => $page,
            'count' => $postModel->countSearch($search, 'OR'),
            'textSearch' => $textSearch,
        ]);
    }

    /**
     * Liste des articles avec scroll
     * Pas d'article chargé via cette méthode : tout sera fait par ajax
     */
    public function scrollList() {
        $this->show('post/scroll_list');
    }

    /**
     * Affiche les détails d'un article
     * @param $id
     */
    public function details($id)
    {
        $postModel = new PostModel();
        $this->show('post/details', ['post' => $postModel->find($id)]);
    }
}