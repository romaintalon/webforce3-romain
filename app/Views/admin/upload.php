<?php
/**
 * @var $result array : le tableau des résultats, contenant 2 tableaux : inserted et updated
 */
$inserted = $result['inserted'];
$updated = $result['updated'];
?>

<?php $this->layout('layout') ?>

<?php $this->start('main_content') ?>

<h1>Résultat de l'upload</h1>

<div class="upload-result inserted">
    <h2><?=sizeof($inserted)?> articles insérés</h2>
    <ul>
        <?php foreach($inserted as $inserted_item): ?>
            <li>
                <?=$inserted_item['id']?>: <?=$inserted_item['title']?>
            </li>
        <?php endforeach ?>
    </ul>
</div>

<div class="upload-result uploaded">
    <h2><?=sizeof($updated)?> articles uploadés</h2>
    <ul>
        <?php foreach($updated as $updated_item): ?>
            <li>
                <?=$updated_item['id']?>: <?=$updated_item['title']?>
            </li>
        <?php endforeach ?>
    </ul>
</div>

<?php $this->stop('main_content') ?>
