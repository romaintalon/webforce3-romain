<?php
    /**
     * @var $error string : l'erreur éventuellement renvoyée lors de l'upload
     */
?>


<?php $this->layout('layout') ?>

<?php $this->start('main_content') ?>
<h1>Formulaire d'upload</h1>

<?php if ($error !== null): ?>
    <div class="form-error">
        <?= $error?>
    </div>
<?php endif ?>

<form method="post"
      enctype="multipart/form-data"
      action="<?= $this->url('admin_upload')?>">

    <div class="form-group">
        <label for="file">Fichier JSON</label>
        <input type="file" name="file" accept=".json">
    </div>

    <button type="submit" class="btn btn-default">Uploader</button>

</form>

<?php $this->stop('main_content')?>
