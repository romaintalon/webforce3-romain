<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title><?= $this->e($title) ?></title>

	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/flexslider.min.css">
	<link rel="stylesheet" href="<?= $this->assetUrl('css/style.css') ?>">

	<script type="text/javascript" src="//code.jquery.com/jquery-1.12.4.min.js"
	<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.js"></script>

	<?= $this->section('specific_script') ?>
</head>
<body>
	<div role="navigation" class="navbar navbar-default navbar-static-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button"
						class="navbar-toggle collapse"
						data-toggle="collapse"
						data-target="#webforce3-navbar-collapse"
						aria-expanded="false"
				>
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>

				</button>
				<a class="navbar-brand">Analyse Webforce3</a>
			</div>

			<div class="collapse navbar-collapse" id="webforce3-navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="<?=$this->url('post_home')?>">Accueil</a></li>
					<li><a href="<?=$this->url('post_page', ['page' => 1])?>">Liste des articles(back)</a></li>
					<li><a href="<?=$this->url('post_scroll_list')?>">Liste des articles(front)</a></li>
					<li><a href="<?=$this->url('admin_form')?>">Upload JSON</a></li>
				</ul>
			</div>
		</div>

	</div>

		<section>
			<div class="container">
				<?= $this->section('main_content') ?>
			</div>
		</section>

		<footer>
		</footer>
</body>
</html>