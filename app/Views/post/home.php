<?php
/**
 * @var $posts array : tableau des articles paginé
 */

?>
<?php $this->layout('layout') ?>

<?php $this->start('specific_script') ?>
<script type="text/javascript" charset="utf-8">
    $(window).load(function() {
        $('.flexslider').flexslider();
    });
</script>
<?php $this->stop('specific_script')?>

<?php $this->start('main_content') ?>
    <h1>Derniers articles parus</h1>
    <div class="flexslider">
        <ul class="slides">
            <?php foreach($posts as $post): ?>
                <li>
                    <div class="post">
                        <div class="title">
                            <h1>
                                <?=$post['title']?>
                            </h1>
                        </div>
                        <div class="author">
                            <?=$post['author']?>
                        </div>
                        <div class="date_add">
                            <!-- Affichage de la date formatée. Par défaut, timezone définie sur UTC -->
                            <?=(new DateTime($post['date_add'], new DateTimeZone('UTC')))->format('d/m/Y H:i:s')?>
                        </div>
                        <div class="content">
                            <?=$post['content']?>
                        </div>
                    </div>                </li>
            <?php endforeach ?>
        </ul>
    </div>
<?php $this->stop('main_content')?>
