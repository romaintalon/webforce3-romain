<?php
/**
 * @var $posts array : tableau des articles paginé
 * @var $page integer : page actuellement affichée
 * @var $count integer : nombre total de posts
 * @var $textSearch string : recherche effectuée
 */
// la dernière page : arrondi supérieur du nombre de post total sur le nombre de post par page.
$lastPage = ceil($count / getApp()->getConfig('post_per_page'));
// la route est différente selon qu'on a une recherche ou non en cours.
$paginationRoute = $textSearch ? 'post_search' : 'post_page';

// Ce serait bien mieux de pouvoir charger une extension
// mais le système actuel ne permet pas de le faire sans
// dupliquer le code contenu dans Controller::show.
function highlight($toHighlight, $originalText) {
    if (strlen($toHighlight) > 0) {
        return str_replace($toHighlight, "<span class='highlight'>{$toHighlight}</span>", $originalText);
    } else {
        return $originalText;
    }
}

?>
<?php $this->layout('layout') ?>

<?php $this->start('main_content') ?>
<h1>Liste des articles (avec pagination)</h1>

<form class="form-inline"
      action="<?=$this->url('post_search', ['page' => 1])?>"
      method="get">
    <div class="form-group">
        <label for="textSearch">Recherche :</label>
        <input type="text"
               class="form-control"
               name="textSearch"
               value="<?=$textSearch?>"/>
    </div>
    <button type="submit" class="btn btn-default">Chercher</button>
</form>

<p class="post-count">
    <?=$count?> articles correspondent à la recherche
</p>

<div class="post-list">
    <?php foreach ($posts as $post): ?>
        <div class="post">
            <div class="title">
                <h2>
                    <a href="<?=$this->url('post_details', ['id' => $post['id']])?>">
                        <?=highlight($textSearch, $post['title'])?>
                    </a>
                </h2>
            </div>
            <div class="author">
                <?=highlight($textSearch, $post['author'])?>
            </div>
            <div class="date_add">
                <!-- Affichage de la date formatée. Par défaut, timezone définie sur UTC -->
                <?=(new DateTime($post['date_add'], new DateTimeZone('UTC')))->format('d/m/Y H:i:s')?>
            </div>
            <div class="content">
                <?=highlight($textSearch, substr($post['content'], 0, 150))?>
                <?=(strlen($post['content']) > 0) ? "..." : ""?>
            </div>
        </div>
        <hr/>
    <?php endforeach ?>
</div>

<nav>
    <ul class="pagination">
        <!-- bouton précédent - désactivé pour la première page -->
        <li class="<?=$page == 1 ? 'disabled' : ''?>">
            <a href="<?=$this->url($paginationRoute, ['page' => $page - 1])?>?textSearch=<?=urlencode($textSearch)?>">
                <span aria-hidden="true">&laquo;</span>
            </a>
        </li>

        <!-- liste des pages -->
        <? for ($x = 1; $x <= $lastPage; $x++): ?>
            <li class="<?=$page == $x ? 'active' : ''?>">
                <a href="<?=$this->url($paginationRoute, ['page' => $x])?>?textSearch=<?=urlencode($textSearch)?>">
                    <?=$x?>
                </a>
            </li>
        <? endfor ?>

        <!-- bouton suivant - désactivé pour la dernière page -->
        <li class="<?=$page == $lastPage ? 'disabled' : ''?>">
            <a href="<?=$this->url($paginationRoute, ['page' => $page + 1])?>?textSearch=<?=urlencode($textSearch)?>">
                <span aria-hidden="true">&raquo;</span>
            </a>
        </li>
    </ul>
</nav>
<?php $this->stop('main_content')?>
