
<?php $this->layout('layout') ?>

<?php $this->start('main_content') ?>
<h1>Liste des articles (avec scroll)</h1>
<div id="post-finder">
    <input class="typeahead form-control" type="text" placeholder="Titre de l'article"/>
</div>

<div class="post-list">
</div>
<div class="load-more">
    Chargement en cours ...
</div>
<?php $this->stop('main_content') ?>

<?php $this->start('specific_script') ?>

<!-- TYPEAHEAD -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // création du typeahead
        $('#post-finder').find('.typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'posts',
            source: new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: '/api/post/search/%QUERY',
                    wildcard: '%QUERY'
                }
            }),
            limit: 10,
            display: function (data) {
                return data.title;
            },
            templates: {
                suggestion: function(data) {
                    return '<div class="suggestion">' + data.title + '</div>';
                }
            }
        });

        // quand un élément est sélectionné, redirection vers la page de détail
        $('#post-finder').find('.typeahead').bind('typeahead:selected', function(event, obj) {
            window.location.href = '/post/' + obj.id;
        });
    });

</script>

<!-- LOADING ON SCROLL -->
<script type="text/javascript">
    $(document).ready(function() {
        var load = false; // pour éviter que plusieurs chargement se fassent en même temps.
        var page = 0;
        var allLoaded = false;
        var offset;

        // charge plus de posts
        function loadMorePosts() {
            $('.load-more').show();
            load = true;
            $.ajax({
                url: "/api/post/" + page, // difficile d'utiliser le routeur du framework, comme la variable page est sur javascript.
                type: "get",
                success: function (data) {
                    if (data.length > 0) {
                        for (x = 0; x < data.length; x++) {
                            // un outil de render serait mieux tout de même
                            var post = $("<div class='post'></div>");
                            post.append("<div class='title'><h2><a href='/post/" + data[x].id + "'>"
                                + data[x].title + "</a></h2></div>");
                            post.append("<div class='author'>" + data[x].author + "</div>");
                            post.append("<div class='date_add'>" + data[x].date_add + "</div>"); // TODO: formatter la date
                            post.append("<div class='content'>" + data[x].content + "</div>");
                            $('.post-list').append(post);
                            $('.post-list').append('<hr/>');
                        }
                    } else {
                        $('.post-list').after("<div class='no-more-post'>Fin des résultats</div>");
                        allLoaded = true;
                    }
                    load = false;
                    page++;
                    $('.load-more').fadeOut(500);
                    offset = $('.post:last').offset();
                }
            });
        }

        loadMorePosts(); // on commence par charger les X premiers articles

        // capter l'évènement de scroll et vérifier si on est en bas ?
        $(window).scroll(function() {
            // TODO: corriger la condition insuffisante pour les ipad / iphone
            // cf https://buzut.fr/2012/10/11/creer-un-scroll-infini-en-jquery/
            if ((offset.top-$(window).height() <= $(window).scrollTop()
                    && load==false && allLoaded==false)) {
                loadMorePosts();
            }
        });
    })
</script>

<?php $this->stop('specific_script') ?>
