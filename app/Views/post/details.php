<?php
/**
 * @var $post : L'article à afficher.
 */

?>

<? $this->layout('layout') ?>

<? $this->start('main_content') ?>

<div class="post">
    <div class="title">
        <h1>
            <?=$post['title']?>
        </h1>
    </div>
    <div class="author">
        <?=$post['author']?>
    </div>
    <div class="date_add">
        <!-- Affichage de la date formatée. Par défaut, timezone définie sur UTC -->
        <?=(new DateTime($post['date_add'], new DateTimeZone('UTC')))->format('d/m/Y H:i:s')?>
    </div>
    <div class="content">
        <?=$post['content']?>
    </div>
</div>

<? $this->stop('main_content') ?>
