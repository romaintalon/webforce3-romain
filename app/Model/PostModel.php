<?php

namespace Model;

use \W\Model\Model;

/**
 * Class PostModel
 * @package Model
 *
 * Beaucoup de code dupliqué dans cette classe,
 * Pour bien faire les choses il faudrait peut être améliorer la classe Model
 * afin de génériser l'utilisation de la recherche (comme dans search()) 
 */
class PostModel extends Model
{
    /**
     * Retourne le nombre de post correspondant à la recherche
     * @param array $search
     * @param string $operator
     * @param bool $stripTags
     * @return bool
     */
    public function countSearch(array $search = array(), $operator = 'OR', $stripTags = true) {
        $sql = "SELECT count(*) as count FROM {$this->table}";

        // Sécurisation de l'opérateur
        $operator = strtoupper($operator);
        if($operator != 'OR' && $operator != 'AND'){
            die('Error: invalid operator param');
        }

        // création du where (duplique Model::search)
        if (sizeof($search) > 0) {
            $sql .= ' WHERE';
            foreach ($search as $key => $value) {
                $sql .= " $key LIKE :$key ";
                $sql .= $operator;
            }
            // Supprime les caractères superflus en fin de requète
            if ($operator == 'OR') {
                $sql = substr($sql, 0, -3);
            } elseif ($operator == 'AND') {
                $sql = substr($sql, 0, -4);
            }
        }

        $sth = $this->dbh->prepare($sql);

        // (duplique Model::search)
        foreach($search as $key => $value){
            $value = ($stripTags) ? strip_tags($value) : $value;
            $sth->bindValue(':'.$key, '%'.$value.'%');
        }

        if(!$sth->execute()){
            return false;
        }

        return $sth->fetch()['count'];
    }

    /**
     * basé sur Model::search, ajoute la notion de pagination et de tri
     * @param string $orderBy
     * @param string $orderDir
     * @param null $limit
     * @param null $offset
     * @param array $search
     * @param string $operator
     * @param bool $stripTags
     * @return array|bool
     */
    public function searchWithPagination($orderBy = '', $orderDir = 'ASC', $limit = null, $offset = null,
            array $search = array(), $operator = 'OR', $stripTags = true) {

        // Sécurisation de l'opérateur
        $operator = strtoupper($operator);
        if($operator != 'OR' && $operator != 'AND'){
            die('Error: invalid operator param');
        }

        // création du where (duplique Model::search)
        $sql = 'SELECT * FROM ' . $this->table;

        if (sizeof($search) > 0) {
            $sql .= ' WHERE';
            foreach ($search as $key => $value) {
                $sql .= " $key LIKE :$key ";
                $sql .= $operator;
            }
            // Supprime les caractères superflus en fin de requète
            if ($operator == 'OR') {
                $sql = substr($sql, 0, -3);
            } elseif ($operator == 'AND') {
                $sql = substr($sql, 0, -4);
            }
        }

        // création de order / limit (duplique Model::findAll)
        if (!empty($orderBy)){

            //sécurisation des paramètres, pour éviter les injections SQL
            if(!preg_match('#^[a-zA-Z0-9_$]+$#', $orderBy)){
                die('Error: invalid orderBy param');
            }
            $orderDir = strtoupper($orderDir);
            if($orderDir != 'ASC' && $orderDir != 'DESC'){
                die('Error: invalid orderDir param');
            }
            if ($limit && !is_int($limit)){
                die('Error: invalid limit param');
            }
            if ($offset && !is_int($offset)){
                die('Error: invalid offset param');
            }

            $sql .= ' ORDER BY '.$orderBy.' '.$orderDir;
            if($limit){
                $sql .= ' LIMIT '.$limit;
                if($offset){
                    $sql .= ' OFFSET '.$offset;
                }
            }
        }

        $sth = $this->dbh->prepare($sql);

        // (duplique Model::search)
        foreach($search as $key => $value){
            $value = ($stripTags) ? strip_tags($value) : $value;
            $sth->bindValue(':'.$key, '%'.$value.'%');
        }

        if(!$sth->execute()){
            return false;
        }
        return $sth->fetchAll();
    }
}