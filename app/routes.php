<?php
	
	$w_routes = array(
		['GET', '/', 'Post#home', 'post_home'],
		['GET', '/scroll-list', 'Post#scrollList', 'post_scroll_list'],
		['GET', '/[:page]', 'Post#search', 'post_page'],
		['GET', '/search/[:page]', 'Post#search', 'post_search'],
		['GET', '/post/[:id]', 'Post#details', 'post_details'],
		['GET', '/admin/form', 'Admin#form', 'admin_form'],
		['POST', '/admin/form', 'Admin#upload', 'admin_upload'],

		['GET', '/api/post/[i:page]', 'PostRest#getList', 'post_rest_get_list'],
		['GET', '/api/post/search/[:textSearch]', 'PostRest#search', 'post_rest_search'],
	);