create table post(
  id INT NOT NULL AUTO_INCREMENT,
  title VARCHAR(255),
  content MEDIUMTEXT,
  date_add DATETIME,
  author VARCHAR(255),
  PRIMARY KEY(id)
);
